# Omeka S - Sindominio.net

Versión adaptada para la integración de __Omeka S__ en la infraestructura de _Sindominio_

Desarrollada para su uso en el repositorio _Hacer Laboratorio_ disponible en https://hacerlaboratorio.sindominio.net

Basada en tres imagenes Docker publicadas en el registry de Sindominio

* MariaDB: Base de datos 
* PHPFPM: Código de Omeka S ejecutable a través de PHP FPM 7
* NGINX: Servidor web para acceder como proxy a PHPFPM y a los archivos no PHP de la aplicación de _Omeka S_

# Configuración

Configurar correctamente las variables de la base de datos _MYSQL_ y los volumenes.

Por defecto, los volúmenes se cargan en el espacio por defecto de Docker.

# Ejemplo Docker Compose

```

version: '3'

networks:
  omekan:
    external: false

volumes:
  db:
  data:          

services:

  db:
    image: registry.sindominio.net/mariadb
    #build: ./mariadb
    container_name: db
    networks:
      - omekan
    volumes:
     - db:/var/lib/mysql
    environment:
      MYSQL_DATABASE: omeka_sd
      MYSQL_USER: omeka_sd
      MYSQL_PASSWORD: omeka_sd

  phpfpm:
    image: registry.sindominio.net/php-omeka-s
    #build: ./php-omeka-s
    container_name: phpfpm
    networks:
      - omekan
    links:
      - db
    volumes:
     - data:/sindominio
    environment:
      MYSQL_DATABASE: omeka_sd
      MYSQL_USER: omeka_sd
      MYSQL_PASSWORD: omeka_sd
      MYSQL_HOST: db

  nginx:
    image: registry.sindominio.net/nginx-omeka-s
    #build: "./nginx-omeka-s/"
    container_name: nginx
    restart: always
    depends_on:
      - phpfpm
    networks: 
     - omekan
    links:
     - phpfpm
    ports:
        - "8090:80"
    volumes:
            - data:/sindominio


```

