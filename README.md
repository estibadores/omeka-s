# Omeka S PHPFPM Docker Image

Imágen de docker de _Omeka S_ con PHPFPM

Levant el puerto 9000 para el proxy PHPFPM con el usuario _omeka_ 

Más info : https://omeka.org/s/

## Configuración

Variables de entorno: 

* MYSQL_DATABASE omeka
* MYSQL_USER omeka
* MYSQL_PASSWORD omeka
* MYSQL_HOST db

Si hemos usado esta imagen en el docker-compose junto con la imagen de _mariadb_ debemos escribir los mismos datos.

## Uso en Docker Compose

```
...

omeka:
    image: registry.sindominio.net/omeka-s
    restart: always
    container_name: sd_omeka_s
		environment:
      MYSQL_ROOT_PASSWORD: omeka
      MYSQL_DATABASE: omeka
      MYSQL_USER: omeka
      MYSQL_PASSWORD: omeka
    links:
      - db
    volumes:
     - data:/sindominio/omeka-s

...


volumes:
 data:

```
